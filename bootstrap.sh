#!/usr/bin/env bash

PHPDIR=/etc/php/7.0
DBNAME=jstest
DBUSERNM=root
DBPASSWD=toor

echo -e "\n--- Ready to install... ---\n"
echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Installing base packages ---\n"
apt-get -y install curl build-essential unzip git > /dev/null 2>&1

echo -e "\n--- Updating packages list - Again ---\n"
apt-get -qq update

echo -e "\n--- Installing MySQL (packages and settings) ---\n"
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
apt-get -y install mysql-server > /dev/null 2>&1

echo -e "Create database"
mysql -u $DBUSERNM -p$DBPASSWD -e "CREATE DATABASE $DBNAME CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;"

echo -e "\n--- Installing Nginx packages and config ---\n"
apt-get -y install nginx > /dev/null 2>&1
ufw allow 'Nginx HTTP' > /dev/null 2>&1

echo -e "\n--- Installing PHP ---\n"
apt-get -y install php php-fpm php-xml php-mysql php-curl php-gd php-mcrypt php-apcu php-mbstring > /dev/null 2>&1
apt-get -y install php-dev > /dev/null 2>&1

sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/" $PHPDIR/fpm/php.ini

echo -e "\n--- Configuring servers ---\n"
systemctl restart php7.0-fpm
rm /etc/nginx/sites-available/default
cp /vagrant/provision/config/nginx-config /etc/nginx/sites-available/default
# sudo nginx -t
systemctl reload nginx
sed -i 's/user = www-data/user = vagrant/g' $PHPDIR/fpm/pool.d/www.conf
sed -i 's/group = www-data/group = vagrant/g' $PHPDIR/fpm/pool.d/www.conf

echo -e "\n--- Installing Composer ---\n"
cd ~
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv composer.phar /usr/local/bin/composer

echo -e "\n--- Installing project dependencies ---\n"
cd /vagrant/www
# composer install --no-plugins --no-scripts
composer install

echo -e "\n--- Installing Drush launcher ---\n"
cd ~
curl -OL https://github.com/drush-ops/drush-launcher/releases/download/0.6.0/drush.phar
chmod +x drush.phar
mv drush.phar /usr/local/bin/drush

echo -e "\n--- Importing base database ---\n"
# if [ -f /vagrant/provision/d8.database.sql ]; then
#   cd /vagrant/www/web
#   drush sql-cli < /vagrant/provision/d8.database.sql
# fi
if [ -f /vagrant/provision/backup.sql ]; then
  cd /vagrant/www/web
  drush sql-cli < /vagrant/provision/backup.sql
  drush cr
fi

# echo -e "\n--- Installing XDebug ---\n"
# cd ~
# git clone git://github.com/xdebug/xdebug.git
# cd xdebug
# phpize > /dev/null 2>&1
# ./configure --enable-xdebug
# make > /dev/null 2>&1
# make install > /dev/null 2>&1
# cp /vagrant/provision/config/xdebug.ini /etc/php/7.0/mods-available/xdebug.ini

echo -e "\n--- Restarting servers ---\n"
systemctl restart php7.0-fpm
systemctl reload nginx

echo -e "\n--- Node JS (8.x) --- 1/2"
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -  > /dev/null 2>&1
echo -e "--- Node JS (8.x) --- 2/2\n"
apt-get install -y nodejs > /dev/null 2>&1

echo -e "\n--- Installing Gulp CLI ---\n"
npm install --global gulp-cli

# echo -e "\n--- Installing theme dependencies ---\n"
# cd /vagrant/www/web/themes/custom/jstest
# npm install
# npm run start
# npm run prod




