<?php

namespace Drupal\tmdb\Controller\Batch\Model;

/**
 * Class to process content bulk export.
 */
class Term {

  /**
   * Function to get or create a basic term.
   */
  public static function getTerm($vocabulary_id, $migration_id, $name, $create = TRUE) {
    $term = NULL;
    $id = \Drupal::service('entity.query')
      ->get('taxonomy_term')
      ->condition('vid', $vocabulary_id)
      ->condition('field_tmdb_id', $migration_id)
      ->execute();

    if (empty($id) && $create === TRUE) {
      $term = \Drupal::service('entity_type.manager')
        ->getStorage('taxonomy_term')
        ->create(['vid' => $vocabulary_id]);
    }
    else {
      $term = \Drupal::service('entity.manager')
        ->getStorage('taxonomy_term')
        ->load(reset($id));
    }

    if (!empty($term)) {
      if (!empty($name)) {
        $term->set('name', $name);
      }
      $term->set('field_tmdb_id', $migration_id);
    }

    return $term;
  }

}
