<?php

namespace Drupal\tmdb\Controller\Batch;

use Drupal\Core\Render\Markup;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\tmdb\Controller\Batch\Model\Content;
use Drupal\tmdb\Controller\Batch\Model\Term;
use Tmdb\ApiToken;
use Tmdb\Client;
use Tmdb\Repository\GenreRepository;
use Tmdb\Repository\MovieRepository;
use Tmdb\Repository\PeopleRepository;

/**
 * Class to process content bulk export.
 */
class Import {

  /**
   * Manage batch tasks for export.
   *
   * @param string $app_key
   *   A string with the api_key value.
   * @param array $options
   *   An array with options.
   */
  public static function init($app_key, array $options) {

    $cardinality = 10;
    $imagesStorage = FieldStorageConfig::loadByName('node', 'field_images');
    if (is_object($imagesStorage)) {
      $cardinality = $imagesStorage->getCardinality();
      if ($cardinality == -1) {
        $cardinality = 10;
      }
    }

    \Drupal::service('state')
      ->set('actor_images_cardinality', $cardinality);

    $popular = (isset($options['source']) && $options['source'] == 'popular');
    $start_page = isset($options['page']) ? intval($options['page']) : 1;
    $max_pages = isset($options['total_pages']) ? intval($options['total_pages']) : 1;
    $update_actor = isset($options['update_actor']) ? boolval($options['update_actor']) : FALSE;
    $num_actors = isset($options['num_actors']) ? intval($options['num_actors']) : 1;

    $operations = [];

    $token = new ApiToken($app_key);
    $client = new Client($token, [
      'cache' => [
        'enabled' => TRUE,
      ],
    ]);

    $repository = new MovieRepository($client);

    for ($page = $start_page; $page < ($start_page + $max_pages); $page++) {

      $collection = self::getMovieCollection($repository, $page, $popular);
      $movies = $collection->getAll();
      foreach ($movies as $movie) {
        $data = self::getMovieBasicData($movie);
        $operations[] = [
          [get_called_class(), 'processMovie'],
          [
            $app_key,
            [
              'update_actor' => $update_actor,
              'num_actors' => $num_actors,
            ],
            $data,
          ],
        ];
      }
    }

    $batch = [
      'title' => t('Importing'),
      'operations' => $operations,
      'finished' => [get_called_class(), 'finishCallback'],
    ];
    batch_set($batch);
  }

  /**
   * Get popular or upcoming movie list.
   */
  public static function getMovieCollection($repository, $page = 1, $popular = TRUE) {
    if ($popular === TRUE) {
      $collection = $repository->getPopular(['page' => $page]);
    }
    else {
      $collection = $repository->getUpcoming(['page' => $page]);
    }
    return $collection;
  }

  /**
   * Convenience function to get information from movie.
   */
  public static function getMovieBasicData($movie) {
    $genres = [];
    foreach ($movie->getGenres()->toArray() as $genre) {
      $genres[] = $genre->getId();
    }

    $companies = [];
    foreach ($movie->getProductionCompanies()->toArray() as $company) {
      $companies[] = $company->getId();
    }

    $data = [
      'id' => $movie->getId(),
      'title' => $movie->getOriginalTitle(),
      'genres' => $genres,
      'website' => $movie->getHomepage(),
      'popularity' => $movie->getPopularity(),
      'posterPath' => $movie->getPosterPath(),
      'originalLang' => $movie->getOriginalLanguage(),
      'releaseDate' => $movie->getReleaseDate()->format('Y-m-d'),
      'releaseYear' => $movie->getReleaseDate()->format('Y'),
    ];

    return $data;
  }

  /**
   * Batch process step to import single movie information.
   */
  public static function processMovie($app_key, $options, $data, &$context) {
    $context['message'] = 'Cleaning: ' . $data['title'];

    $img_base_path = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2';

    if (empty($context['results']['log'])) {
      $context['results']['log'] = [];
    }

    // Movie fill step-by-step.
    $movie = Content::getContent('movie', $data['id'], $data['title']);

    if (!empty($movie)) {
      $movie->save();
      $movie->set('field_release_date', $data['releaseDate']);
      $movie->set('field_year', $data['releaseYear']);
      $movie->set('field_original_language', $data['originalLang']);

      $client = self::clientInstance($app_key);
      $api_movie_repository = new MovieRepository($client);
      $api_movie = $api_movie_repository->load($data['id']);

      if (!is_null($api_movie)) {
        // Credits process start.
        $api_credits = $api_movie->getCredits();
        if (!is_null($api_credits)) {
          $api_collection_cast = $api_credits->getCast();

          if (!is_null($api_collection_cast)) {
            $cast = [];
            $actor_limit = $options['num_actors'];
            foreach ($api_collection_cast as $person) {
              // Limit added to reduce API consumption.
              if ($actor_limit > 0) {
                $actor_limit--;
              }
              else {
                break;
              }

              $actor = Content::getContent('actor', $person->getId(), $person->getName());
              if (!empty($actor)) {

                if ($actor->isNew() === TRUE || ($options['update_actor'] === TRUE)) {
                  $api_people_repository = new PeopleRepository($client);
                  $api_person = $api_people_repository->load($person->getId());

                  if (!is_null($api_person)) {
                    $birthday = ($api_person->getBirthday() instanceof \DateTime) ? $api_person->getBirthday()->format('Y-m-d') : NULL;
                    $deathday = ($api_person->getDeathday() instanceof \DateTime) ? $api_person->getDeathday()->format('Y-m-d') : NULL;

                    $actor->set('body', $api_person->getBiography());
                    $actor->set('field_birthday', $birthday);
                    $actor->set('field_deathday', $deathday);
                    $actor->set('field_birth_place', $api_person->getPlaceOfBirth());
                    $actor->set('field_popularity', intval($api_person->getPopularity()));
                    $actor->set('field_website', $api_person->getHomepage());
                    $actor->set('field_portrait', $img_base_path . $api_person->getProfilePath());

                    // Actor credits.
                    $credit_paragraph = \Drupal::service('entity_type.manager')
                      ->getStorage('paragraph')
                      ->create(['type' => 'credit']);
                    $credit_paragraph->set('field_movie', [['target_id' => $movie->id()]]);
                    $credit_paragraph->set('field_character_name', $person->getCharacter());
                    if ($credit_paragraph->save()) {
                      $actor->set('field_movies',
                        [
                          [
                            'target_id' => $credit_paragraph->id(),
                            'target_revision_id' => $credit_paragraph->getRevisionId(),
                          ],
                        ]
                      );
                    }

                    // Actor images.
                    $api_images = $api_person->getImages();
                    $images = [];
                    $cardinality = \Drupal::service('state')
                      ->get('actor_images_cardinality');
                    foreach ($api_images as $key => $value) {
                      $images[] = [
                        'uri' => $img_base_path . $value->getFilePath(),
                      ];
                      $cardinality--;
                      if ($cardinality == 0) {
                        break;
                      }
                    }
                    $actor->set('field_images', $images);
                  }
                  $actor->save();
                }

                $cast[] = ['target_id' => $actor->id()];
              }
            }
            $movie->set('field_actors', $cast);
          }
        }

        // Poster image.
        $api_poster_image = $api_movie->getPosterImage();
        if (!is_null($api_poster_image)) {
          $movie->set('field_poster', $img_base_path . $api_poster_image->getFilePath());
        }

        $movie->set('field_popularity', intval($api_movie->getPopularity()));
        $movie->set('body', $api_movie->getOverview());
      }

      // Genres process start.
      $genres = [];
      foreach ($data['genres'] as $genre_id) {
        $genre = Term::getTerm('genres', $genre_id, '', FALSE);
        if (is_null($genre)) {
          $client = self::clientInstance($app_key);
          $api_genre_repository = new GenreRepository($client);
          $api_genre = $api_genre_repository->load($genre_id);

          if (!is_null($api_genre)) {
            $genre = Term::getTerm('genres', $genre_id, $api_genre->getName());
            if (!empty($genre)) {
              $genre->save();
              $genres[] = ['target_id' => $genre->id()];
            }
          }
        }
        else {
          $genre->save();
          $genres[] = ['target_id' => $genre->id()];
        }
      }
      $movie->set('field_genres', $genres);

      // Production Companies process start.
      $api_companies = $api_movie->getProductionCompanies();
      $companies = [];
      foreach ($api_companies as $api_company) {
        $company = Term::getTerm('production_companies', $api_company->getId(), '', FALSE);
        if (is_null($company)) {

          if (!is_null($api_company)) {
            $company = Term::getTerm('production_companies', $api_company->getId(), $api_company->getName());
            if (!empty($company)) {
              $company->save();
              $companies[] = ['target_id' => $company->id()];
            }
          }
        }
        else {
          $company->save();
          $companies[] = ['target_id' => $company->id()];
        }
      }
      $movie->set('field_production_companies', $companies);

      // Videos process start.
      $api_videos = $api_movie->getVideos();
      foreach ($api_videos as $api_video) {
        $movie->set('field_trailer', $api_video->getUrl());
        break;
      }

      // Alternative titles process start.
      $api_titles = $api_movie->getAlternativeTitles();
      $titles = [];
      foreach ($api_titles as $api_title) {
        $titles[] = ['value' => $api_title->getTitle()];
      }
      $movie->set('field_other_names', $titles);

      // Similar moview process start.
      $api_similars = $api_movie->getSimilar();
      $similars = [];
      foreach ($api_similars as $api_similar) {
        $similar = Content::getContent('movie', $api_similar->getId(), '', FALSE);
        if (!is_null($similar)) {
          $similars[] = ['target_id' => $similar->id()];
        }
      }
      $movie->set('field_similar_movies', $similars);

      // Reviews.
      $api_reviews = $api_movie->getReviews();
      $reviews = [];
      foreach ($api_reviews as $api_review) {
        $values = [
          'entity_type' => 'node',
          'entity_id'   => $movie->id(),
          'field_name'  => 'field_reviews',
          'uid' => 0,
          'comment_type' => 'review',
          'subject' => $api_review->getMediaTitle(),
          'comment_body' => $api_review->getContent(),
          'field_score' => rand(10, 50),
          'status' => 1,
        ];
        $comment = \Drupal::service('entity_type.manager')
          ->getStorage('comment')
          ->create($values);
        $comment->save();
      }

      $movie->save();
    }
  }

  /**
   * Singleton function to get client instance.
   */
  public static function clientInstance($app_key) {
    static $client = NULL;
    if ($client === NULL) {
      $token = new ApiToken($app_key);
      $client = new Client($token,
        [
          'cache' => [
            'enabled' => TRUE,
          ],
        ]
      );
    }
    return $client;
  }

  /**
   * Implements callback_batch_finished().
   */
  public static function finishCallback($success, $results, $operations) {
    if ($success) {
      $message = t('Import process finished.');

      if (!empty($results['log']) && count($results['log'] > 0)) {
        $message .= t('<br />log: @log.',
          [
            '@log' => \Drupal::service('serialization.json')->encode($results['log']),
          ]
        );
      }

      \Drupal::service('messenger')
        ->addStatus(Markup::create($message));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments',
        [
          '%error_operation' => $error_operation[0],
          '@arguments' => print_r($error_operation[1], TRUE),
        ]
      );
      \Drupal::service('messenger')
        ->addError($message);
    }
  }

}
