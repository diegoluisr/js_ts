<?php

namespace Drupal\tmdb\Controller\Batch\Model;

/**
 * Class to process content bulk export.
 */
class Content {

  /**
   * Function to get or create a basic node.
   */
  public static function getContent($bundle, $migration_id, $title, $create = TRUE) {
    $node = NULL;
    $id = \Drupal::service('entity.query')
      ->get('node')
      ->condition('type', $bundle)
      ->condition('field_tmdb_id', $migration_id)
      ->execute();

    if (empty($id) && $create === TRUE) {
      $node = \Drupal::service('entity_type.manager')
        ->getStorage('node')
        ->create(['type' => $bundle]);
    }
    else {
      $node = \Drupal::service('entity.manager')
        ->getStorage('node')
        ->load(reset($id));
    }

    if (!empty($node)) {
      if (!empty($title)) {
        $node->set('title', $title);
      }
      $node->set('field_tmdb_id', $migration_id);
      $node->set('status', TRUE);
    }

    return $node;
  }

}
