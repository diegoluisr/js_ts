<?php

namespace Drupal\tmdb\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'link_to_image' formatter.
 *
 * @FieldFormatter(
 *   id = "link_to_image",
 *   label = @Translation("Link to image"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkToImageFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays a link as image.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $uri = $item->getUrl()->getUri();
      // Render each element as markup.
      $element[$delta] = [
        '#markup' => '<img src="' . $uri . '" />',
      ];
    }
    return $element;
  }

}
