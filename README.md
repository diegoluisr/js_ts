JS_TS
==========
The following code is a technical test solution.

Why Vagrant?
------------
Vagrant is a virtualization tool that allows a team to work in a homogeneous environment, trying to copy all possibles configuration of the production context to avoid or reduce any possible error or misconfiguration.

Requirements
------------
In order to run this project, you require to have installed the latest version of...
 * [Vagrant](https://www.vagrantup.com/)
 * [VirtualBox](https://www.virtualbox.org/)

Setup
-----
Open your terminal and run the below commands

Clone the repository
```bash
$ git clone git@bitbucket.org:diegoluisr/js_ts.git
```
Change directory
```bash
$ cd js_ts
```
Run (vagrant and virtual box required) This command will execute **ALL TASKS** related to set ready the working environment. 
```bash
$ vagrant up
```
Then you could go into the virtualized machine using next command
```bash
$ vagrant ssh
```
Theme
-----
To compile the SASS files of the theme, execute below commands inside guest machine.
```bash
$ cd /vagrant/www/web/themes/custom/jstest
$ npm install
$ npm run prod
```

Modules
-------
This project have two modules "tmdb" and "tmdb_config"

### TMDB
Have all logic related to import information throug TMDB API

### TMDB CONFIG
Have all data configuration that allows data importation. This module is an implementation of "Features" module for Drupal 8, that uses the configuration system to do all more easy.

The Movie Database
------------------
After Vagrant provisioning ends, you could access below URL.
[http://localhost:8282/](http://localhost:8282/)

You could login using this credentials
```bash
user: admin
pass: admin
```
Then you could access Configuration > System > The Movie Database
If you don't have an API_KEY you must request it to [TMDB](https://developers.themoviedb.org/3/getting-started/introduction)

Then you could import movies, actors, genres and production companies using the Batch option.


Code Quality Testing
--------------------
I'm using Drupal standard for code quality testing purposes
```bash
$ cd /vagrant/www
$ ./vendor/bin/phpcs --standard=Drupal --extensions=js --ignore=*/node_modules/*,/*build/*,*/vendor/* /vagrant/www/web/themes/custom/jstest/
$ ./vendor/bin/phpcs --standard=Drupal --extensions=php,module,theme --ignore=*/node_modules/*,/*build/*,*/vendor/* /vagrant/www/web/themes/custom/jstest/
$ ./vendor/bin/phpcs --standard=Drupal --extensions=php,module,theme --ignore=*/node_modules/*,/*build/*,*/vendor/* /vagrant/www/web/modules/custom/tmdb/
```

Finally
-------
Stop vagrant
```bash
$ vagrant halt
```
Destroy the virtualized machine.
```bash
$ vagrant destroy
```
